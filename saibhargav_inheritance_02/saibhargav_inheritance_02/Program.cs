﻿using System;

namespace saibhargav_inheritance_02
{
    class Vehicle
    {
        public virtual String Fuel()
        {
            return "Petrol";
        }
    }
    class Car : Vehicle
    {
        public override String Fuel()
        {
            return "Diesel";
        }
    }
    class Bus: Vehicle
    {
        public override string Fuel()
        {
            return "CNG";
        }
    }
    class Program
    {
        static void Main()
        {
            Car c = new Car();
            Console.WriteLine(c.Fuel());
            Bus b = new Bus();
            Console.WriteLine(b.Fuel());


        }
    }
}
