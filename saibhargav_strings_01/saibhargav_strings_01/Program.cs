﻿using System;

namespace saibhargav_strings_01
{
    class Program
    {
        static void Main()
        {
            String s1 = "Hello";
            String s2 = "World";
            Console.WriteLine("First String: " + s1);
            Console.WriteLine("Second String: " + s2);
            Console.WriteLine("Joining of two strings: " + string.Concat(s1, s2));
        }
    }
}
