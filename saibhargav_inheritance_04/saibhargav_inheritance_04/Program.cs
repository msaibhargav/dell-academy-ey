﻿using System;

namespace saibhargav_inheritance_04
{
    class Polygon
    {

        public void Perimeter(int length, int sides)
        {

            int result = length * sides;
            Console.WriteLine("Perimeter: " + result);
        }
    }

    class Square : Polygon
    {

        public int length = 200;
        public int sides = 4;
        public void Area()
        {

            int area = length * length;
            Console.WriteLine("Area of Square: " + area);
        }
    }

    class Rectangle : Polygon
    {

        public int length = 100;
        public int breadth = 200;
        public int sides = 4;

        public void Area()
        {

            int area = length * breadth;
            Console.WriteLine("Area of Rectangle: " + area);
        }
    }

    class Program
    {

        static void Main(string[] args)
        {

            Square s1 = new Square();
            s1.Area();
            s1.Perimeter(s1.length, s1.sides);


            Rectangle t1 = new Rectangle();
            t1.Area();
            t1.Perimeter(t1.length, t1.sides);

        }
    }
}
