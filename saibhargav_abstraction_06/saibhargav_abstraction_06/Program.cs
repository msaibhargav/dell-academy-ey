﻿using System;

namespace saibhargav_abstraction_06
{

    public abstract class Figure
    {
        public double Width, Height, Radius;
        public const float pi = 3.14f;
        public abstract double Area();
    }
    public class Rectangle : Figure
    {
        public Rectangle(double Width, double Height)
        {
            this.Width = Width;
            this.Height = Height;
        }
        public override double Area()
        {
            return Width * Height;
        }
    }
    public class Circle : Figure
    {
        public Circle(double Radius)
        {
            this.Radius = Radius;
        }
        public override double Area()
        {
            return pi * Radius * Radius;
        }
    }
    class Cone : Figure
    {
        public Cone(double Height,double Radius)
        {
            this.Height = Height;
            this.Radius = Radius;
        }
        public override double Area()
        {
            return pi * Radius * (Radius + Math.Sqrt(Height * Height + Radius * Radius));

        }
    }
    class Program
    {
        static void Main()
        {
            Rectangle rectangle = new Rectangle(12.67, 56.78);
            Console.WriteLine("Area of rectangle: " + rectangle.Area());
            Circle circle = new Circle(45.67);
            Console.WriteLine("Area of circle: " + circle.Area());
            Cone cone = new Cone(34.98,12.98);
            Console.WriteLine("Area of Cone: " + cone.Area());
        }
    }
}
