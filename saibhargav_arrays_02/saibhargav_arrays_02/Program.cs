﻿using System;
using System.Linq;

namespace saibhargav_arrays_02
{
    class Program
    {
        static void Main()
        {
            int[] x = {14, 55, 22, 57, 99};
            Console.WriteLine("Smallest in x: " + x.Min());
            Console.WriteLine("Largest in x: " + x.Max());
        }
    }
}
