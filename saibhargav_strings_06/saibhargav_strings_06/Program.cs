﻿using System;

namespace saibhargav_strings_06
{
    class Program
    {
        static void Main(string[] args)
        {
            String s1 = "Hello";
            char[] ch = { 'a', 'b', 'c', 'd', 'e' };
            String s2 = new string(ch);
            Console.WriteLine(s1);
            Console.WriteLine(s2);
        }
    }
}
