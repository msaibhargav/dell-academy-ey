﻿using System;

namespace saibhargav_abstraction_03
{
    abstract class Shape
    {
        public abstract int Total_area();
    }
    class Square_Class : Shape
    {
        private int Length=0;
        public Square_Class(int x)
        {
            Length = x;
        }
        public override int Total_area()
        {
            Console.Write("Total area of Square Class: ");
            return (Length * Length);
        }
    }
    class Program
    {
        static void Main()
        {
            Shape sh = new Square_Class(10);
            double result = sh.Total_area();
            Console.Write(result);

        }
    }
}
