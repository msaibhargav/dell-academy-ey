﻿using System;

namespace saibhargav_arrays_06
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] a = new int[5, 2] { { 0, 0 }, { 1, 2 }, { 3, 4 }, { 3, 5 }, { 2, 2 } };
        
            for(int i = 0; i < 5; i++)
            {
                for(int j = 0; j < 2; j++)
                {
                    Console.WriteLine("a[{0},{1}]={2}", i, j, a[i, j]);
                }
            }
        }
    }
}
