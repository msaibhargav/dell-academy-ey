﻿using System;

namespace saibhargav_inheritance_05
{
    class Parent
    {
        public virtual void F1()
        {
            Console.WriteLine("Inside f1");
        }
    }
    class Child: Parent
    {
       public override void F1()
        {
            base.F1();
            Console.WriteLine("Inside child f1");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Child c = new Child();
            c.F1();
        }
    }
}
