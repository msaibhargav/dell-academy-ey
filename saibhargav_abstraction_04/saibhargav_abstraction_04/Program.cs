﻿using System;

namespace saibhargav_abstraction_04
{
    abstract class BMW
    {
       public void Common()
        {
            Console.WriteLine("Common");
        }
        public abstract void Accelerate();
    }
    class ThreeSeries : BMW
    {
        public override void Accelerate()
        {
            Console.WriteLine("ThreeSeries - Accelerate");
        }
    }
    class FiveSeries : BMW
    {
        public override void Accelerate()
        {
            Console.WriteLine("FiveSeries - Accelerate");
        }
    }
    class Program
    {
        static void Main()
        {
            ThreeSeries x = new ThreeSeries();
            x.Accelerate();
            x.Common();
            FiveSeries f = new FiveSeries();
            f.Accelerate();
            f.Common();
        }
    }
}
