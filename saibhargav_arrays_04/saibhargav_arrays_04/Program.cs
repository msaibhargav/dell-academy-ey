﻿using System;

namespace saibhargav_arrays_04
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] x = { 33, 42, 45, 21, 32, 4, 13 };
            Array.Sort(x);
            for (int i = 0; i < x.Length; i++)
            {
                Console.WriteLine(x[i]);
            }
            String[] s = { "Honda", "BMW", "Ford", "Volvo" };
            Array.Sort(s);
            for (int i = 0; i < s.Length; i++)
            {
                Console.WriteLine(s[i]);
            }
        }
    }
}
