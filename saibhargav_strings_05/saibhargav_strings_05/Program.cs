﻿using System;

namespace saibhargav_strings_05
{
    class Program
    {
        static void Main(string[] args)
        {
            String s1 = "Hello";
            String s2 = "World";
            String s3 = "Hello";
            Console.WriteLine("Compare s1 and s2: " + s1.Equals(s2));
            Console.WriteLine("Compare s1 and s3: " + s1.Equals(s3));
        }
    }
}
