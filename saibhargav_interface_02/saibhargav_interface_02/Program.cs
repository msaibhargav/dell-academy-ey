﻿using System;

namespace saibhargav_interface_02
{
    interface Car
    {
        void Go();
        void Stop();
    }
    class Honda : Car
    {
        public void Go()
        {
            Console.WriteLine("Honda - Go");
        }
        public void Stop()
        {
            Console.WriteLine("Honda - Stop");
        }
    }
    class Program
    {
        static void Main() 
        {
            Honda honda = new Honda();
            honda.Go();
            honda.Stop();
        }
    }
}
