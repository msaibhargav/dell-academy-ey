﻿using System;
using System.Linq;
namespace saibhargav_arrays_03
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] x = { 11, 17, 34, 57, 98 };
            float sum = x.Sum();
            int count = x.Count();
            Console.WriteLine("The sum of numbers: " + sum);
            Console.WriteLine("Average of numbers: " + x.Average());
        }
    }
}
