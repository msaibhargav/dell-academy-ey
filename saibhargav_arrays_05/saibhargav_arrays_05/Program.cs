﻿using System;

namespace saibhargav_arrays_05
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = { 12, 3, 53, 23 };
            for(int i = a.Length - 1; i >= 0; i--)
            {
                Console.Write(a[i]+" ");
            }
        }
    }
}
