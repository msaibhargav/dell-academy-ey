﻿using System;

namespace saibhargav_strings_09
{
    class Program
    {
        static void Main(string[] args)
        {
            String s = "Hello World";
            String[] r = s.Split(" ");
            for(int i = 0; i < r.Length; i++)
            {
                Console.WriteLine(r[i]);
            }
            Console.WriteLine("After Replace: " + s.Replace('l', 'k'));
        }
    }
}
