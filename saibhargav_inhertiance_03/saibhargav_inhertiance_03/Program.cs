﻿using System;

namespace saibhargav_inhertiance_03
{
   

   
        
        class Animal
        {
            public virtual void Sound()
            {

                Console.WriteLine("Slient");
            }
        }

       
        class Dog : Animal
        {

          
            public override void Sound()
            {

                Console.WriteLine("Bark Bark");
            }
        }
        class Program
        {

            static void Main(string[] args)
            {
                
                Dog d = new Dog();
                d.Sound();
                
            }
        }
    
}
