﻿using System;

namespace saibhargav_strings_04
{
    class Program
    {
        static void Main(string[] args)
        {
            String s = "Hello World";
            Console.WriteLine("The first character in the string :" + s[0]);
            Console.WriteLine("Index of e: " + s.IndexOf("e"));
            Console.WriteLine("SubString: " + s.Substring(6));
        }
    }
}
