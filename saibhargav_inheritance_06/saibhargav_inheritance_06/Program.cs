﻿using System;

namespace saibhargav_inheritance_06
{
    class Parent
    {
        public void F1()
        {
            Console.WriteLine("Inside f1");
        }
    }
    class Child : Parent
    {
        public void F2()
        {

            Console.WriteLine("Inside child f2");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Child c = new Child();
            c.F1();
            c.F2();
        }
    }
}
