﻿using System;

namespace saibhargav_strings_02
{
    class Program
    {
        static void Main()
        {
            String s = "Hello World";
            Console.WriteLine("The length of the String: " + s.Length);
            Console.WriteLine("Convert to Upper case: " + s.ToUpper());
            Console.WriteLine("Convert to Lower Case: " + s.ToLower());

        }
    }
}
