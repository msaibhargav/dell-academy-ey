﻿using System;

namespace saibhargav_strings_08
{
    class Program
    {
        static void Main(string[] args)
        {
            String s = "Hello";
            String result = "";
            for(int i = s.Length - 1; i >= 0; i--)
            {
                result = result + s[i];

            }
            Console.WriteLine("Reverse the string: "+result);
        }
    }
}
