﻿using System;

namespace saibhargav_strings_10
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime date = new DateTime(2022, 4, 1);
            string str = date.ToString("M");
            Console.WriteLine("Date and time: {0}", date);
            Console.WriteLine("Month and Date: {0}", str);

        }
    }
}
