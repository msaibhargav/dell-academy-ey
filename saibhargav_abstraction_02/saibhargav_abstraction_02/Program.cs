﻿using System;
using System.Collections.Generic;
using System.Text;

namespace saibhargav_abstraction_02
{
    abstract class Animal
    {
        public abstract void Sound();
        public abstract void Eat();
    }
    class Dog : Animal
    {
        public override void Sound()
        {
            Console.WriteLine("dog sounds bark bark");
        }
        public override void Eat()
        {
            Console.WriteLine("dog eats biscuits");
        }
    }
    class Program
    {
        static void Main()
        {
            Dog d = new Dog();
            d.Sound();
            d.Eat();

        }
    }
}