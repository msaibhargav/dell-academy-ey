﻿using System;

namespace saibhargav_abstraction
{
    abstract class AbsParent
    {
        public void add(int x,int y)
        {
            Console.WriteLine(x + y);
        }
        public void sub(int x,int y)
        {
            Console.WriteLine(x - y);
        }
        public abstract void mul(int x, int y);
        public abstract void div(int x, int y);
    }
    class AbsChild : AbsParent
    {
        public override void mul(int x, int y)
        {
            Console.WriteLine(x * y);
        }
        public override void div(int x, int y)
        {
            Console.WriteLine(x / y);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            AbsChild absChild = new AbsChild();
            absChild.add(4, 5);
            absChild.sub(4, 5);
            absChild.mul(4, 5);
            absChild.div(4, 5);
            Console.ReadLine(); 
        }
    }
}
