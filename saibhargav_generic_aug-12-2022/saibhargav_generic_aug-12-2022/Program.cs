﻿using System;
using System.IO;

namespace saibhargav_generic_aug_12_2022
{
    public class SomeClass
    {
        public void GenericMethods<T1,T2>(T1 Param1, T2 Param2)
        {
            Console.WriteLine($"Parameter 1:{Param1}:Parameter 2:{Param2}");

        }
    }
    class program
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine("generics methods");
            SomeClass s = new SomeClass();
            s.GenericMethods(10,"abc");
            s.GenericMethods("hii","k");
            Console.WriteLine();
            
        }
    }
}
