﻿using System;

namespace saibhargav_strings_07
{
    class Program
    {
        static void Main(string[] args)
        {
            String s1 = "abc";
            String s2 = "xyz";
            String s3 = "abc";
            String s4 = new string("abc");
            Console.WriteLine(s1 == s3);
            Console.WriteLine(s1.Equals(s3));
            Console.WriteLine(s1 == s2);
            Console.WriteLine(s1.Equals(s2));
            Console.WriteLine(s1 == s4);
            Console.WriteLine(s1.Equals(s4));
        }
    }
}
