﻿using System;

namespace saibhargav_interface_01
{
    interface Shape
    {
        void Area(double r);
    }
    class Circle : Shape
    {
        public void Area(double r)
        {
            Console.WriteLine("Area of circle: "+Math.PI * r * r);
        }
    }
    class Program
    {
        static void Main()
        {
            Circle circle = new Circle();
            circle.Area(7.8);
        }
    }
}
